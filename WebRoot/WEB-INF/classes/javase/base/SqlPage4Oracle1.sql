SELECT
  *
FROM (SELECT
        T1.*,
        ROWNUM rn
      FROM (SELECT
              *
            FROM testTable
            ORDER BY id DESC) T1
      WHERE ROWNUM <= 20)
WHERE rn > 0;