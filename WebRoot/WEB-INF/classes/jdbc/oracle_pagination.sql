select * from (select t1.*,rownum rn from (select column1,column2 from test order by column1,column2 desc) where rownum <= 50) where rn > 0

create index test_index on test(column1,column2 desc); 

select * from (select t1.*,rownum rn from (select /*+index(test_index) */ column1,column2 from test order by column1,column2 desc) where rownum <= 50) where rn > 0

select * from test t1,( select rid from ( select rownum rn,t.rid from ( select /*+index(test_index) */ rownum rid from test order by column1,column2 desc ) t where rownum <= 50 ) where rn > 0) t2 where t1.rowid = t2.rid