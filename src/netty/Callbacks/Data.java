package netty.Callbacks;

/**
 * User: Realfighter
 * Date: 2014/8/19
 * Time: 22:16
 */
public class Data {
    private int n;
    private int m;

    public Data(int n, int m) {
        this.n = n;
        this.m = m;
    }

    @Override
    public String toString() {
        int r = n / m;
        return n + " / " + m + " = " + r;
    }
}
