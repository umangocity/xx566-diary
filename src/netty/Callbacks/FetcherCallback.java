package netty.Callbacks;

/**
 * User: Realfighter
 * Date: 2014/8/19
 * Time: 22:15
 */
public interface FetcherCallback {
    void onData(Data data) throws Exception;

    void onError(Throwable cause);
}
