package netty.Callbacks;

/**
 * User: Realfighter
 * Date: 2014/8/19
 * Time: 22:12
 */
public interface Fetcher {
    void fetchData(FetcherCallback callback);
}
