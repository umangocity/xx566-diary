package netty.Callbacks;

/**
 * User: Realfighter
 * Date: 2014/8/19
 * Time: 22:20
 */
public class MyFetcher implements Fetcher {
    final Data data;

    public MyFetcher(Data data) {
        this.data = data;
    }

    @Override
    public void fetchData(FetcherCallback callback) {
        try {
            callback.onData(data);
        } catch (Exception e) {
            callback.onError(e);
        }
    }
}
