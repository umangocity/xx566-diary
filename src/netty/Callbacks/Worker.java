package netty.Callbacks;

/**
 * User: Realfighter
 * Date: 2014/8/19
 * Time: 22:11
 */
public class Worker {
    private void doWork() {
        Fetcher fetcher = new MyFetcher(new Data(1, 0));
        fetcher.fetchData(new FetcherCallback() {
            @Override
            public void onData(Data data) throws Exception {
                System.out.println("Data received : "+ data);
            }

            @Override
            public void onError(Throwable cause) {
                System.out.println("Throwable occur : "+ cause.getMessage());
            }
        });
    }

    public static void main(String[] args){
        Worker worker = new Worker();
        worker.doWork();
    }

}
