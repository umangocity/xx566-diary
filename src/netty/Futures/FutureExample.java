package netty.Futures;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * User: Realfighter
 * Date: 2014/8/19
 * Time: 22:32
 */
public class FutureExample {

    public static void main(String[] args) throws Exception {
        ExecutorService executorService = Executors.newCachedThreadPool();
        Runnable task1 = new Runnable() {
            @Override
            public void run() {
                System.out.println("this is task1");
            }
        };
        Callable<Integer> task2 = new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return new Integer(100);
            }
        };
        Future<?> f1 = executorService.submit(task1);
        Future<Integer> f2 = executorService.submit(task2);
        System.out.println("f1 is done?" + f1.isDone());
        System.out.println("f2 is done?" + f2.isDone());
        while (f1.isDone()) {
            System.out.println("f1 is done");
            break;
        }
        while (f2.isDone()) {
            System.out.println("f2 returns value:" + f2.get());
            break;
        }
    }

}
