package javase.concurrency;

public class SyncTest {

	private static Object lockObject = new Object();

	static int x;

	static int y;

	private static class Thread1 extends Thread {

		@Override
		public void run() {
			synchronized (lockObject) {
				x = y = 0;
				System.out.println("Thread1 >>>> " + x);
			}
		}

	}

	private static class Thread2 extends Thread {

		@Override
		public void run() {
			synchronized (lockObject) {
				x = y = 1;
				System.out.println("Thread2 >>>> " + y);
			}
		}

	}

	public static void main(String[] args) {
		new Thread1().start();
		new Thread2().start();
	}

}
