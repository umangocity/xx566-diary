package javase.concurrency;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * 并发编程
 * 
 * @Package javase
 * @ClassName: ExecutorTest
 * @author Realfighter
 * @date 2014-7-9 下午12:35:40
 */
public class ExecutorTest {

	public static void main(String[] args) {
		ExecutorTest.testExecutor();
	}

	/**
	 * // 创建一个可根据需要创建新线程的线程池 
	 * Executors.newCachedThreadPool(); 
	 * // 创建一个使用单个 worker线程的 
	 * Executor Executors.newSingleThreadExecutor(); 
	 * // 创建一个可重用线程数为10的线程池 
	 * int nThreads = 10;// 可重用线程数为10
	 * Executors.newFixedThreadPool(nThreads); 
	 * //创建一个线程池，它可安排在给定延迟后运行命令或者定期地执行 
	 * int corePoolSize = 10;// 固定保存10个线程在池内
	 * Executors.newScheduledThreadPool(corePoolSize);
	 * 
	 * @Title: testExecutor
	 * @author Realfighter
	 * @date 2014-7-9 下午01:14:00 void
	 */
	public static void testExecutor() {
		ExecutorService pool = Executors.newSingleThreadExecutor();
		Callable<String> c = new Callable<String>() {
			// Callable会默认调用call()方法
			public String call() throws Exception {
				return "hello,relafighter";
			}
		};
		//submit()方法提交一个 Runnable任务用于执行，并返回一个表示该任务的 Future
		Future<String> f = pool.submit(c);
		try {
			//get()方法获取线程执行后的结果
			System.out.println(f.get());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		//只有手动调用shutdown()才能关闭线程池
		pool.shutdown();
	}

}
