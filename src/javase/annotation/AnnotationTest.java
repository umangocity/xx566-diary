package javase.annotation;

import java.lang.reflect.Method;

@Authority(describe = "注解类测试")
public class AnnotationTest {

	@Authority(name = "test", describe = "注解方法测试")
	public void test() {
		System.out.println("hello annotation");
	}

	public static void main(String[] args) {
		// 获取当前线程所在class
		Class<?> clazz = AnnotationTest.class;
		// 获取含有指定注解的类
		Authority authorityClass = (Authority) clazz
				.getAnnotation(Authority.class);
		if (authorityClass != null) {
			System.out.println(authorityClass.describe());
			Method[] methods = clazz.getDeclaredMethods();
			for (Method method : methods) {
				// 判断方法上是否存在指定的注解
				if (method.isAnnotationPresent(Authority.class)) {
					Authority authorityMethod = method
							.getAnnotation(Authority.class);
					System.out.println(authorityMethod.describe());
				}
			}
		}
	}
	
}