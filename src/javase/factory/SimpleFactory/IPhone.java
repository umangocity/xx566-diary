package javase.factory.SimpleFactory;

/**
 * Iphone手机实例
 * User: Realfighter
 * Date: 2014/8/23
 * Time: 22:52
 */
public class IPhone implements Mobile {
    @Override
    public String brand() {
        return "Iphone";
    }

    @Override
    public double price() {
        return 5000.0;
    }
}
