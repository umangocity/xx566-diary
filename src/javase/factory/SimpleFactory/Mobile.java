package javase.factory.SimpleFactory;

/**
 * 抽象手机设备接口
 * User: Realfighter
 * Date: 2014/8/23
 * Time: 22:49
 */
public interface Mobile {

    public String brand();//手机品牌

    public double price();//手机价格

}
