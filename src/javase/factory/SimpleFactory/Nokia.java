package javase.factory.SimpleFactory;

/**
 * Nokia手机实例
 * User: Realfighter
 * Date: 2014/8/23
 * Time: 22:52
 */
public class Nokia implements Mobile {
    @Override
    public String brand() {
        return "Nokia";
    }

    @Override
    public double price() {
        return 3000.0;
    }
}
