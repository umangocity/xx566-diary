package javase.factory.SimpleFactory;

/**
 * 手机供应商
 * User: Realfighter
 * Date: 2014/8/23
 * Time: 22:58
 */
public class Supplier {

    public static void main(String[] args) {
        //手机供应商需要Nokia手机
        Mobile nokia = Foxconn.produce("Nokia");
        System.out.println(nokia.price());//3000.0
        //手机供应商需要Iphone手机
        Mobile iphone = Foxconn.produce("Iphone");
        System.out.println(iphone.price()); //5000.0
    }

}
