package javase.factory.SimpleFactory;

/**
 * 富士康工厂
 * User: Realfighter
 * Date: 2014/8/23
 * Time: 22:54
 */
public class Foxconn {

    /**
     * 生产手机方法
     *
     * @return
     */
    public static Mobile produce(String brand) {
        if ("Nokia".equals(brand)) {
            return new Nokia();
        } else if ("Iphone".equals(brand)) {
            return new IPhone();
        }
        return null;
    }

}
