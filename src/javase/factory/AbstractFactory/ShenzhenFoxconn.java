package javase.factory.AbstractFactory;

/**
 * 深圳富士康生产Iphone和IPad
 * User: Realfighter
 * Date: 2014/8/26
 * Time: 12:09
 */
public class ShenzhenFoxconn implements Foxconn {
    @Override
    public Mobile produceMobile() {
        return new Iphone();
    }

    @Override
    public Pad producePad() {
        return new Ipad();
    }
}
