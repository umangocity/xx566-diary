package javase.factory.AbstractFactory;

/**
 * Mobile系列：Nokia
 * User: Realfighter
 * Date: 2014/8/26
 * Time: 12:04
 */
public class Nokia implements Mobile{
    @Override
    public String brand() {
        return "Nokia";
    }

    @Override
    public double price() {
        return 3000.0;
    }

    //这里为了方便调试，重写了toString
    @Override
    public String toString() {
        return this.brand() + "：" + this.price();
    }
}
