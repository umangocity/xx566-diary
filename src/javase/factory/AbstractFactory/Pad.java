package javase.factory.AbstractFactory;

/**
 * User: Realfighter
 * Date: 2014/8/26
 * Time: 11:57
 */
public interface Pad {

    public double size();//尺寸

    public String face();//外观

}
