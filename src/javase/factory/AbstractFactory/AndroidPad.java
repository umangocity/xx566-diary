package javase.factory.AbstractFactory;

/**
 * Pad系列：AndroidPad
 * User: Realfighter
 * Date: 2014/8/26
 * Time: 12:06
 */
public class AndroidPad implements Pad {
    @Override
    public double size() {
        return 10.4; //10.4寸
    }

    @Override
    public String face() {
        return "better";
    }

    //这里为了方便调试，重写了toString
    @Override
    public String toString() {
        return "AndroidPad：" + this.size() + "，" + this.face();
    }
}
