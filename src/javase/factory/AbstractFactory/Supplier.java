package javase.factory.AbstractFactory;

/**
 * Mobile和Pad供应商
 * User: Realfighter
 * Date: 2014/8/26
 * Time: 12:22
 */
public class Supplier {

    public static void main(String[] args) {
        Foxconn foxconn;//富士康
        Mobile mobile;//手机
        Pad pad;//平板
        //首先来到郑州富士康
        foxconn = new ZhengzhouFoxconn();
        //拿到生产的手机
        mobile = foxconn.produceMobile();
        //验货
        System.out.println(mobile.toString());// Nokia：3000.0
        //拿到生产的pad
        pad = foxconn.producePad();
        //验货
        System.out.println(pad.toString()); // AndroidPad：10.4，better
        //同样的来到深圳富士康，拿到手机和pad，验货
        foxconn = new ShenzhenFoxconn();
        mobile = foxconn.produceMobile();
        System.out.println(mobile.toString());// Iphone：5000.0
        pad = foxconn.producePad();
        System.out.println(pad.toString()); // Ipad：9.7，best
    }

}
