package javase.factory.AbstractFactory;

/**
 * Pad系列：Ipad
 * User: Realfighter
 * Date: 2014/8/26
 * Time: 12:07
 */
public class Ipad implements Pad {
    @Override
    public double size() {
        return 9.7; //9.7寸
    }

    @Override
    public String face() {
        return "best";
    }

    //这里为了方便调试，重写了toString
    @Override
    public String toString() {
        return "Ipad：" + this.size() + "，" + this.face();
    }
}
