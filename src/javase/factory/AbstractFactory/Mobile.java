package javase.factory.AbstractFactory;

/**
 * 产品系列：Mobile
 * User: Realfighter
 * Date: 2014/8/26
 * Time: 11:56
 */
public interface Mobile {

    public String brand();//品牌

    public double price();//手机价格
}
