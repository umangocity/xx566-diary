package javase.factory.AbstractFactory;

/**
 * Mobile系列：Iphone
 * User: Realfighter
 * Date: 2014/8/26
 * Time: 12:06
 */
public class Iphone implements Mobile {
    @Override
    public String brand() {
        return "Iphone";
    }

    @Override
    public double price() {
        return 5000.0;
    }

    //这里为了方便调试，重写了toString
    @Override
    public String toString() {
        return this.brand() + "：" + this.price();
    }
}
