package javase.factory.AbstractFactory;

/**
 * 抽象的foxconn工厂
 * User: Realfighter
 * Date: 2014/8/26
 * Time: 12:01
 */
public interface Foxconn {

    public Mobile produceMobile();//生产手机

    public Pad producePad();//生产pad

}
