package javase.factory.AbstractFactory;

/**
 * 郑州富士康生产Nokia和AndroidPad
 * User: Realfighter
 * Date: 2014/8/26
 * Time: 12:02
 */
public class ZhengzhouFoxconn implements Foxconn {
    @Override
    public Mobile produceMobile() {
        return new Nokia();
    }

    @Override
    public Pad producePad() {
        return new AndroidPad();
    }
}
