package javase.factory.FactoryMethod;

/**
 * Nokia手机工厂
 * User: Realfighter
 * Date: 2014/8/25
 * Time: 21:09
 */
public class NokiaFactory implements MobileFactory {
    @Override
    public Mobile produce() {
        return new Nokia();
    }
}
