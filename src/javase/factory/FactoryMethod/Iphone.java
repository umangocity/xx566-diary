package javase.factory.FactoryMethod;

/**
 * Iphone手机实例
 * User: Realfighter
 * Date: 2014/8/25
 * Time: 21:08
 */
public class Iphone implements Mobile {
    @Override
    public String brand() {
        return "Iphone";
    }

    @Override
    public double price() {
        return 5000.0;
    }

    //这里为了方便调试，重写了toString
    @Override
    public String toString() {
        return this.brand() + "：" + this.price();
    }
}
