package javase.factory.FactoryMethod;

/**
 * 抽象出的手机工厂
 * User: Realfighter
 * Date: 2014/8/25
 * Time: 21:08
 */
public interface MobileFactory {

    /**
     * 生产手机
     * @return
     */
    Mobile produce();

}
