package javase.factory.FactoryMethod;

/**
 * 抽象手机设备接口
 * User: Realfighter
 * Date: 2014/8/25
 * Time: 21:08
 */
public interface Mobile {

    public String brand();//手机品牌

    public double price();//手机价格

}
