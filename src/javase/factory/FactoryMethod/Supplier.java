package javase.factory.FactoryMethod;

/**
 * 手机供应商
 * User: Realfighter
 * Date: 2014/8/23
 * Time: 21:08
 */
public class Supplier {

    public static void main(String[] args) {
        //首先找到Nokia手机工厂
        MobileFactory factory = new NokiaFactory();
        //Nokia工厂开始生产手机
        Mobile mobile = factory.produce();
        System.out.println(mobile.toString()); //Nokia：3000.0
        //再找到Iphone手机工厂
        factory = new IphoneFactory();
        //Iphone工厂开始生产手机
        mobile = factory.produce();
        System.out.println(mobile.toString());//Iphone：5000.0
    }

}
