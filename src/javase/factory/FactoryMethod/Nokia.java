package javase.factory.FactoryMethod;

/**
 * Nokia手机实例
 * User: Realfighter
 * Date: 2014/8/25
 * Time: 21:08
 */
public class Nokia implements Mobile {
    @Override
    public String brand() {
        return "Nokia";
    }

    @Override
    public double price() {
        return 3000.0;
    }

    //这里为了方便调试，重写了toString
    @Override
    public String toString() {
        return this.brand() + "：" + this.price();
    }
}
