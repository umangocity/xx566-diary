package javase.factory.FactoryMethod;

/**
 * 苹果手机工厂
 * User: Realfighter
 * Date: 2014/8/25
 * Time: 21:08
 */
public class IphoneFactory implements MobileFactory {
    @Override
    public Mobile produce() {
        return new Iphone();
    }
}
