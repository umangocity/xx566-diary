package javase.decorator;

/**
 * 客户来购买coffee
 * User: Realfighter
 * Date: 2014/8/16
 * Time: 10:45
 */
public class Customer {

    /**
     * @param costs    价格
     * @param contains 成分
     */
    private static void print(int index, double costs, String contains) {
        System.out.println("customer " + index + " >>> coffee costs：" + costs + "，contains：" + contains);
    }

    public static void main(String[] args) {
        //第一杯coffee只是最基本的coffee
        Coffee coffee = new CoffeeOnlyWater();
        print(1, coffee.costs(), coffee.contains());
        //第二杯coffee在前者基础上加了milk
        coffee = new CoffeeAddMilk(coffee);
        print(2, coffee.costs(), coffee.contains());
        //第三杯coffee在第二杯基础上再加了sugar
        coffee = new CoffeeAddSugar(coffee);
        print(3, coffee.costs(), coffee.contains());
    }
}
