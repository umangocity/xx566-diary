package javase.decorator;

/**
 * coffee装饰类
 * User: Realfighter
 * Date: 2014/8/16
 * Time: 10:30
 */
public abstract class CoffeeDecorator extends Coffee {
    protected final Coffee coffee;

    public CoffeeDecorator(Coffee coffee) {
        this.coffee = coffee;
    }

    public double costs() {
        return coffee.costs();
    }

    public String contains() {
        return coffee.contains();
    }

}
