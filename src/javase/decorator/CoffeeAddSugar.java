package javase.decorator;

/**
 * 装饰成分：糖sugar
 * User: Realfighter
 * Date: 2014/8/16
 * Time: 10:43
 */
public class CoffeeAddSugar extends CoffeeDecorator {
    public CoffeeAddSugar(Coffee coffee) {
        super(coffee);
    }

    @Override
    public double costs() {
        //sugar额外加1元
        return super.costs() + 1;
    }

    @Override
    public String contains() {
        return super.contains() + " sugar";
    }
}
