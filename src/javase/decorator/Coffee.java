package javase.decorator;

/**
 * 抽象主体类
 * User: Realfighter
 * Date: 2014/8/16
 * Time: 10:09
 */
public abstract class Coffee {
    abstract double costs(); //coffee价格

    abstract String contains();//coffee包含成分
}
