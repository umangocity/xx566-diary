package javase.decorator;

/**
 * 最基本的成分water
 * User: Realfighter
 * Date: 2014/8/16
 * Time: 10:28
 */
public class CoffeeOnlyWater extends Coffee {
    @Override
    public double costs() {
        return 1;
    }

    @Override
    public String contains() {
        return "water";
    }
}
