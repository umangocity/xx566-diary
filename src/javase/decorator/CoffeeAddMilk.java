package javase.decorator;

/**
 * 装饰成分：牛奶milk
 * User: Realfighter
 * Date: 2014/8/16
 * Time: 10:41
 */
public class CoffeeAddMilk extends CoffeeDecorator {
    public CoffeeAddMilk(Coffee coffee) {
        super(coffee);
    }

    @Override
    public double costs() {
        //milk额外加2元
        return super.costs() + 2;
    }

    @Override
    public String contains() {
        return super.contains() + " milk";
    }
}
