package javase.base;

/**
 * 多线程输出素数
 * 
 * @Package javase
 * @ClassName: CalculatePrimes
 * @author Realfighter
 * @date 2014-7-10 下午03:26:02
 */
public class CalculatePrimesByMultiThread extends Thread {

	public static final int MAX_PRIMES = 1000000;// 输出1000000个

	public static final int TEN_SECONDS = 10000;// 主线程休息10秒

	public volatile boolean finished = false;// 主线程是否被唤醒

	@Override
	public void run() {
		int[] primes = new int[MAX_PRIMES];
		int count = 0;
		for (int i = 2; count < MAX_PRIMES; i++) {
			if (finished) {
				break;
			}
			boolean prime = true;
			for (int j = 0; j < count; j++) {
				if (i % primes[j] == 0) {
					prime = false;
					break;
				}
			}
			if (prime) {
				primes[count++] = i;
				System.out.println("Found Prime：" + i);
			}
		}
	}

	public static void main(String[] args) {
		CalculatePrimesByMultiThread calculatePrimes = new CalculatePrimesByMultiThread();
		calculatePrimes.start();
		try {
			Thread.sleep(TEN_SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		calculatePrimes.finished = true;
	}

}
