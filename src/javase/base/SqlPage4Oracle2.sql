SELECT
  t1.*
FROM testTable t1, (SELECT
                      rid
                    FROM (SELECT
                            ROWNUM rn,
                            t.rid
                          FROM (SELECT
                                  ROWID rid
                                FROM testTable
                                WHERE 1 = 1) t
                          WHERE ROWNUM <= 20)
                    WHERE rn > 0) t2
WHERE 1 = 1 AND t1.ROWID = t2.rid;