package javase.base;

import java.util.Date;

public class LongOrlong {

	public static void main(String[] args) {
		System.out.println(time1());
		System.out.println(time2());
	}

	public static long time1() {
		Date date1 = new Date();
		int sum = 0;
		for (int i = 0; i < Integer.MAX_VALUE; i++) {
			sum += i;
		}
		return new Date().getTime() - date1.getTime();
	}

	public static long time2() {
		Date date1 = new Date();
		Integer sum = 0;
		for (int i = 0; i < Integer.MAX_VALUE; i++) {
			sum += i;
		}
		return new Date().getTime() - date1.getTime();
	}

}
