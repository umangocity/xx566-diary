package javase.base;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * User: Realfighter
 * Date: 2015/6/19
 * Time: 17:01
 */
public class TestIntError {
    /**
     * 测试int计算误差
     */
    @Test
    public void testIntError() {
        int num = 3 / 2 * 2;
        System.out.println("3 / 2 * 2");
        assertThat(num, is(2));
        num = 5 / 2 * 2;
        System.out.println("5 / 2 * 2");
        assertThat(num, is(4));
    }

}
