package javase.base;

public class TenThreads {

	private static class WorkerThread extends Thread {
		int max = Integer.MIN_VALUE;
		int[] ourArray;

		public WorkerThread(int[] ourArray) {
			this.ourArray = ourArray;
		}

		@Override
		public void run() {
			for (int i = 0; i < ourArray.length; i++)
				max = Math.max(max, ourArray[i]);
		}

		public int getMax() {
			return max;
		}
	}

	public static void main(String[] args) {
		WorkerThread[] threads = new WorkerThread[10];
		int[][] bigMatrix = getBigHairyMatrix();
		int max = Integer.MIN_VALUE;
		for (int i = 0; i < 10; i++) {
			threads[i] = new WorkerThread(bigMatrix[i]);
			threads[i].start();
		}
		try {
			for (int i = 0; i < 10; i++) {
				threads[i].join();
				max = Math.max(max, threads[i].getMax());
			}
		} catch (InterruptedException e) {
		}
		System.out.println("Maximum value was " + max);
	}

	static int[][] getBigHairyMatrix() {
		int[][] matrix = new int[10][10];
		for (int i = 0; i < 10; i++)
			for (int j = 0; j < 10; j++) {
				matrix[i][j] = (int) (Math.random() * 1000);
			}
		return matrix;
	}

}
