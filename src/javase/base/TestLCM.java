package javase.base;

import org.junit.Test;

import java.util.Arrays;

/**
 * LCM：最小公倍数
 * User: Realfighter
 * Date: 2015/1/20
 * Time: 14:59
 */
public class TestLCM {

    /**
     * 1-N的最小公倍数
     * LCM：最小公倍数
     * 说明：循环并依次整除，当全部整除时得到的值即为最小公倍数
     */
    @Test
    public void testLCM() {
        int result = 0;
        int countNum = 20;//需要计算的数目：1-20的最小公倍数
        for (int i = 1; i < Integer.MAX_VALUE; i++) {
            int num = countNum;
            while(num > 0) {
                int count = 0;
                for (int j = 1; j <= countNum; j++) {
                    if(i%j!=0){
                        break;
                    }else{
                        count ++;
                    }
                }
                if(count==countNum){
                    result = i;
                    break;
                }
                num --;
            }
            if(result > 0){
                System.out.println("1-"+countNum+"的最小公倍数为："+result);
                break;
            }
        }
    }

    /**
     * 多个不连续数字的最小公倍数
     */
    @Test
    public void testLCM2(){
        int result = 0;
        int[] arrays = {2,4,6,8,10,12,14,16,18,20};//需要计算的数组
        for (int i = 1; i < Integer.MAX_VALUE; i++) {
            int num = arrays.length;
            while(num > 0) {
                int count = 0;
                for(int array : arrays){
                    if(i%array!=0){
                        break;
                    }else{
                        count ++;
                    }
                }
                if(count==arrays.length){
                    result = i;
                    break;
                }
                num --;
            }
            if(result > 0){
                System.out.println(Arrays.toString(arrays)+"的最小公倍数为："+result);
                break;
            }
        }
    }

}