package javase.base;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class FormatTest {
	
	public static void main(String[] args) {
		
		Calendar cal = Calendar.getInstance();

		//这里我们可以通过SimpleDateFormat格式化日期输出
		SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.println(dateFormater.format(cal.getTime()));
		
		//在看完本篇文章后，我们可以这么做
		System.out.println(String.format("%1$tY-%1$tm-%1$te %1$tk:%1$tM:%1$tS", cal));
		
		//也可以这么做
		System.out.println(String.format("%1$tY-%<tm-%<te %<tk:%<tM:%<tS", cal));

		//还可以这么做
		System.out.println(String.format("%1$tF %1$tT", cal));
		
	}
	
}
