package javase.base;

/**
 * 多线程输出素数
 * 
 * @Package javase
 * @ClassName: CalculatePrimes
 * @author Realfighter
 * @date 2014-7-10 下午03:26:02
 */
public class CalculatePrimes extends Thread {

	public static void main(String[] args) {
		int min = 2, max = 100;
		findPrime1(min, max);
		findPrime2(min, max);
	}

	public static void findPrime1(int min, int max) {
		for (int i = min; i <= max; i++) {
			int _count = (int) Math.sqrt(i);
			boolean isprime = true;
			for (int j = 2; j <= _count; j++) {
				if (i % j == 0) {
					isprime = false;
					break;
				}
			}
			if (isprime) {
				System.out.println("CalculatePrimes.findPrime1() find prime : "
						+ i);
			}
		}
	}

	public static void findPrime2(int min, int max) {
		int[] primes = new int[max - min + 2];
		int count = 0;
		for (int i = min; count < max; i++) {
			if (i > max) {
				break;
			}
			boolean prime = true;
			for (int j = 0; j < count; j++) {
				if (i % primes[j] == 0) {
					prime = false;
					break;
				}
			}
			if (prime) {
				primes[count++] = i;
				System.out.println("CalculatePrimes.findPrime2() find prime : "
						+ i);
			}
		}
	}

}
