package javase.base;

import java.util.Arrays;

/**
 * 数组的反转
 * User: Realfighter
 * Date: 2014/9/22
 * Time: 21:53
 */
public class ArrayReverse {

    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5};
        System.out.println(Arrays.toString(reverse1(array)));
        System.out.println(Arrays.toString(reverse2(array)));
    }

    /**
     * 没有考虑效率的反转
     *
     * @param array
     * @return
     */
    private static int[] reverse1(int[] array) {
        int[] newArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            newArray[i] = array[array.length - 1 - i];
        }
        return newArray;
    }

    /**
     * 考虑时间复杂度和空间复杂度的反转
     *
     * @param array
     * @return
     */
    private static int[] reverse2(int[] array) {
        int temp;
        for (int i = 0; i < array.length / 2; i++) {
            temp = array[i];
            array[i] = array[array.length - 1 - i];
            array[array.length - 1 - i] = temp;
        }
        return array;
    }

}
