package javase.adapter;

/**
 * 源角色（Adaptee）
 * User: Realfighter
 * Date: 2014/8/28
 * Time: 21:41
 */
public class Adaptee {

    public void sampleOperation1() {
        System.out.println("this is sampleOperation1()");
    }

}
