package javase.adapter;

/**
 * Target：目标接口
 * User: Realfighter
 * Date: 2014/8/28
 * Time: 21:44
 */
public interface Target {

    public void sampleOperation1();

    public void sampleOperation2();

}
