package javase.adapter.object;

import javase.adapter.Adaptee;
import javase.adapter.Target;

/**
 * 对象的适配器
 * User: Realfighter
 * Date: 2014/8/28
 * Time: 21:56
 */
public class Adapter implements Target {

    //内部包装被适配类的实例
    private Adaptee adaptee;

    //通过构造函数传入具体的需要适配的被适配类
    public Adapter(Adaptee adaptee) {
        this.adaptee = adaptee;
    }

    @Override
    public void sampleOperation1() {
        //使用委托方式
        this.adaptee.sampleOperation1();
    }

    @Override
    public void sampleOperation2() {
        System.out.println("this is sampleOperation2()");
    }
}
