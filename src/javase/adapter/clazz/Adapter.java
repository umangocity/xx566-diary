package javase.adapter.clazz;

import javase.adapter.Adaptee;
import javase.adapter.Target;

/**
 * 类的适配器
 * User: Realfighter
 * Date: 2014/8/28
 * Time: 21:46
 */
public class Adapter extends Adaptee implements Target {
    @Override
    public void sampleOperation2() {
        System.out.println("this is sampleOperation2()");
    }
}
