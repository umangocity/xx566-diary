package javase.adapter;

import javase.adapter.clazz.Adapter;
import org.junit.Test;

/**
 * 客户端测试
 * User: Realfighter
 * Date: 2014/8/28
 * Time: 21:48
 */
public class Client {

    /**
     * 类的适配器模式测试
     */
    @Test
    public void testAdapterOfClass() {
        Target target = new Adapter();
        //this is sampleOperation1()
        target.sampleOperation1();
        //this is sampleOperation2()
        target.sampleOperation2();
    }

    /**
     * 对象的适配器模式
     */
    @Test
    public void testAdapterOfObject() {
        Target target = new javase.adapter.object.Adapter(new Adaptee());
        //this is sampleOperation1()
        target.sampleOperation1();
        //this is sampleOperation2()
        target.sampleOperation2();
    }

}
