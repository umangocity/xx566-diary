package javase.tag;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * 自定义标签：状态Tag 
 * User: Realfighter 
 * Date: 2014/10/12 
 * Time: 23:38
 */
public class StatusTag extends TagSupport {

	private static final long serialVersionUID = 1L;
	
	private Integer value;// 订单状态的真实值

	//重写doStartTag和doEndTag方法
	@Override
	public int doStartTag() throws JspException {
		//JSP API中提供的对象输出类
		JspWriter out = this.pageContext.getOut();
		try {
			String str = Status.getStatus(value);
			out.print(str);
		} catch (Exception e) {
			throw new JspException(e.getMessage());
		}
		// 由于我们的标签没有正文部分，直接返回SKIP_BODY即可，更多返回值见下图
		return SKIP_BODY;
	}

	@Override
	public int doEndTag() throws JspException {
		return EVAL_PAGE;//更多返回值见下图
	}

	public Integer getValue() {
		return value;
	}
	//这里必须有set方法进行赋值
	public void setValue(Integer value) {
		this.value = value;
	}

}

// 这里简单的模拟一下获取订单状态
final class Status {
	public static final Integer BUILD = 1;// 订单建立
	public static final Integer END = 0;// 订单完成
	public final static Map<Integer, String> statusMap = getStatusMap();

	private static Map<Integer, String> getStatusMap() {
		Map<Integer, String> map = new HashMap<Integer, String>() {
			
			private static final long serialVersionUID = 1L;

			{
				put(BUILD, "订单建立");
				put(END, "订单完成");
			}
		};
		return map;
	}

	//获取订单状态
	public static String getStatus(Integer value) {
		return statusMap.get(value);
	}

}