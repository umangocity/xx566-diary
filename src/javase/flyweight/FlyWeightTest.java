package javase.flyweight;

/**
 * 享元模式
 * 
 * @Package javase.flyweight
 * @ClassName: FlyWeightTest
 * @author Realfighter
 * @date 2014-8-3 下午02:08:11
 */
public class FlyWeightTest {

	public static void main(String[] args) {
		Integer i1 = 127;
		Integer i2 = 127;
		System.out.println(i1 == i2);// true
		i1 = 128;
		i2 = 128;
		/**
		 * 翻开源码，我们看到
		 *  public static Integer valueOf(int i) {
				final int offset = 128;
				if (i >= -128 && i <= 127) { // must cache 
			    	return IntegerCache.cache[i + offset];
				}
		        return new Integer(i);
		    }
		 */
		System.out.println(i1 == i2);// false
		Long a1 = -129l;
		Long a2 = -129l;
		/**
		 * 相应的在Long源码里，也有
		 *  public static Long valueOf(long l) {
				final int offset = 128;
				if (l >= -128 && l <= 127) { // will cache
			    	return LongCache.cache[(int)l + offset];
				}
		        return new Long(l);
		    }
		 */
		System.out.println(a1 == a2);// false
	}
}
