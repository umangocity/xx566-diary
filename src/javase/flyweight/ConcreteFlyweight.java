package javase.flyweight;

/**
 * 具体享元对象角色
 * 
 * @Package javase.flyweight
 * @ClassName: ConcreteFlyweight
 * @author Realfighter
 * @date 2014-8-4 下午04:41:58
 */
public class ConcreteFlyweight implements Flyweight {

	private String intrinsicState;// 内部状态

	public ConcreteFlyweight(String intrinsicState) {
		this.intrinsicState = intrinsicState;
	}

	/**
	 * 外部状态传入
	 * 
	 * @Title: operation
	 * @author Realfighter
	 * @date 2014-8-4 下午04:47:30
	 * @param extrinsicState
	 * @return
	 */
	public String operation(String extrinsicState) {
		String str = "内部状态:" + intrinsicState + ";外部状态:" + extrinsicState;
		return str;
	}
}
