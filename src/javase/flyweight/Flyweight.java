package javase.flyweight;

/**
 * 抽象享元角色
 * 
 * @Package javase.flyweight
 * @ClassName: Flyweight
 * @author Realfighter
 * @date 2014-8-4 下午04:39:47
 */
public interface Flyweight {

	/**
	 * extrinsicState：外部状态
	 * 
	 * @Title: operation
	 * @author Realfighter
	 * @date 2014-8-4 下午04:44:42
	 * @param extrinsicState
	 *            String
	 */
	public String operation(String extrinsicState);

}
