package javase.flyweight;

/**
 * 客户端角色
 * 
 * @Package javase.flyweight
 * @ClassName: Client
 * @author Realfighter
 * @date 2014-8-4 下午05:07:00
 */
public class Client {

	FlyweightFactory factory = new FlyweightFactory();
	Flyweight fly1;
	Flyweight fly2;
	Flyweight fly3;
	Flyweight fly4;
	Flyweight fly5;
	Flyweight fly6;

	public Client() {
		// 三种内部状态：u love me1,u love me2,u love me3
		fly1 = factory.getFlyWeight("u love me 1");
		fly2 = factory.getFlyWeight("u love me 2");
		fly3 = factory.getFlyWeight("u love me 3");
		fly4 = factory.getFlyWeight("u love me 1");
		fly5 = factory.getFlyWeight("u love me 2");
		fly6 = factory.getFlyWeight("u love me 3");
	}

	public void doFlyweight() {
		// 六种外部状态i love u 123456
		System.out.println(fly1.operation("i love u 1"));
		System.out.println(fly2.operation("i love u 2"));
		System.out.println(fly3.operation("i love u 3"));
		System.out.println(fly4.operation("i love u 4"));
		System.out.println(fly5.operation("i love u 5"));
		System.out.println(fly6.operation("i love u 6"));
		System.out.println("享元对象数量：" + factory.getFlyweightSize());
	}

	public static void main(String[] args) {
		Client client = new Client();
		client.doFlyweight();
	}

}
