package javase.builder;

/**
 * 工人实例A
 * 
 * @Package javase.builder
 * @ClassName: WorkerA
 * @author Realfighter
 * @date 2014-7-24 下午01:58:24
 */
public class WorkerA implements Worker {
	
	/**
	 * 建造RoomA
	 * 
	 * @Title: buildRoomA
	 * @author Realfighter
	 * @date 2014-7-24 下午02:11:04
	 */
	public void buildRoom() {
		House.INSTANCE.setRoomA(true);
	}

	/**
	 * 交付RoomA
	 * 
	 * @Title: deliverRoomA
	 * @author Realfighter
	 * @date 2014-7-24 下午02:11:12
	 * @return
	 */
	public boolean deliverRoom() {
		return House.INSTANCE.roomA;
	}

}
