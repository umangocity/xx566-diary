package javase.builder;

/**
 * 工人实例B
 * 
 * @Package javase.builder
 * @ClassName: WorkerB
 * @author Realfighter
 * @date 2014-7-24 下午01:58:24
 */
public class WorkerB implements Worker {

	/**
	 * 建造RoomB
	 * 
	 * @Title: buildRoom
	 * @author Realfighter
	 * @date 2014-7-24 下午02:11:04
	 */
	public void buildRoom() {
		House.INSTANCE.setRoomB(true);
	}

	/**
	 * 交付RoomB
	 * 
	 * @Title: deliverRoom
	 * @author Realfighter
	 * @date 2014-7-24 下午02:11:12
	 * @return
	 */
	public boolean deliverRoom() {
		return House.INSTANCE.roomB;
	}

}
