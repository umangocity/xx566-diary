package javase.builder;

/**
 * 房屋主人，负责接收房子
 * 
 * @Package javase.builder
 * @ClassName: HouseOwner
 * @author Realfighter
 * @date 2014-7-24 下午01:51:44
 */
public class HouseOwner {

	/**
	 * @Title: main
	 * @author Realfighter
	 * @date 2014-7-24 下午01:51:34
	 * @param args
	 *            void
	 */
	public static void main(String[] args) {
		// 找一个包工头
		Leader leader = new Leader();
		// 包工头找两个工人
		Worker workerA = new WorkerA();
		Worker workerB = new WorkerB();
		// 包工头指挥两个工人
		leader.order(workerA);
		leader.order(workerB);
		// 包工头将验收后的房屋交付屋主人
		leader.deliverHouse();
	}

}
