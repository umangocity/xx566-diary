package javase.builder;

/**
 * 建造者模式
 * 
 * @Package javase
 * @ClassName: Builder
 * @author Realfighter
 * @date 2014-7-19 下午03:19:43
 */
public class BuilderPattern {

	private final int hight;

	private final int weight;

	private final int length;

	public static class Instance {

		private final int hight;

		private final int weight;

		private int length;

		public Instance(int hight, int weight) {

			this.hight = hight;

			this.weight = weight;

		}

		public Instance length(int val) {
			this.length = val;
			return this;
		}

		public BuilderPattern build() {
			return new BuilderPattern(this);
		}

	}

	private BuilderPattern(Instance instance) {

		this.hight = instance.hight;

		this.weight = instance.weight;

		this.length = instance.length;

	}

	public int getHight() {
		return hight;
	}

	public int getWeight() {
		return weight;
	}

	public int getLength() {
		return length;
	}

	public static void main(String[] args) {
		BuilderPattern builder = new BuilderPattern.Instance(2, 3).length(5).build();
		System.out.println(builder.getHight() + ">>>" + builder.getWeight()
				+ ">>>" + builder.getLength());
	}
}
