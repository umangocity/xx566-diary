package javase.builder;

/**
 * 工作接口
 * 
 * @Package javase.builder
 * @ClassName: Worker
 * @author Realfighter
 * @date 2014-7-24 下午02:04:53
 */
public interface Worker {

	/**
	 * 建造Room
	 * 
	 * @Title: buildRoom
	 * @author Realfighter
	 * @date 2014-7-24 下午02:04:42 void
	 */
	public void buildRoom();

	/**
	 * 交付Room
	 * 
	 * @Title: deliverRoom
	 * @author Realfighter
	 * @date 2014-7-24 下午02:32:32
	 * @return boolean
	 */
	public boolean deliverRoom();

}
