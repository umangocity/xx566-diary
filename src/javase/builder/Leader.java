package javase.builder;

/**
 * 包工头
 * 
 * @Package javase.builder
 * @ClassName: Leader
 * @author Realfighter
 * @date 2014-7-24 下午01:56:24
 */
public class Leader {

	/**
	 * 命令工人
	 * 
	 * @Title: order
	 * @author Realfighter
	 * @date 2014-7-24 下午02:51:27
	 * @param worker
	 *            void
	 */
	public void order(Worker worker) {
		worker.buildRoom();
		worker.deliverRoom();
	}

	/**
	 * 验收交付房屋
	 * 
	 * @Title: deliverHouse
	 * @author Realfighter
	 * @date 2014-7-24 下午02:51:36
	 * @return House
	 */
	public House deliverHouse() {
		if (House.INSTANCE.getRoomA() && House.INSTANCE.getRoomB()) {
			return House.INSTANCE;
		}
		return null;
	}

}
