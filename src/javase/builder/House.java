package javase.builder;

/**
 * 房屋本身
 * 
 * @Package javase.builder
 * @ClassName: House
 * @author Realfighter
 * @date 2014-7-24 下午01:58:12
 */
public enum House {

	INSTANCE;

	boolean roomA;// A屋

	boolean roomB;// B屋

	/**
	 * 建造RoomA
	 * 
	 * @Title: setRoomA
	 * @author Realfighter
	 * @date 2014-7-24 下午02:01:28 void
	 */
	public void setRoomA(boolean roomA) {
		this.roomA = roomA;
	}

	/**
	 * 建造RoomB
	 * 
	 * @Title: setRoomB
	 * @author Realfighter
	 * @date 2014-7-24 下午02:02:14 void
	 */
	public void setRoomB(boolean roomB) {
		this.roomB = roomB;
	}

	public boolean getRoomA() {
		return roomA;
	}

	public boolean getRoomB() {
		return roomB;
	}

}
