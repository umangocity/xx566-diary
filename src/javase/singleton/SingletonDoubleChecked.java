package javase.singleton;

public class SingletonDoubleChecked {
	private static SingletonDoubleChecked INSTANCE = null;

	private SingletonDoubleChecked() {
	}

	public static SingletonDoubleChecked getInstance() {
		if (INSTANCE == null) {
			//检查实例，如果为空，就进入同步区域
			synchronized (SingletonDoubleChecked.class) {
				//进入同步区域后，再检查一次，如果仍为空，才创建实例
				if (INSTANCE == null) {
					INSTANCE = new SingletonDoubleChecked();
				}
			}
		}
		return INSTANCE;
	}
}
