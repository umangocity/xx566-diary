package javase.singleton;

public class SingletonSecond {

	private static final SingletonSecond SINGLETON = new SingletonSecond();

	private SingletonSecond() {
	}

	public static SingletonSecond getInstance() {
		return SINGLETON;
	}

	public void test() {
		System.out.println("SingletonSecond.test()");
	}

}
