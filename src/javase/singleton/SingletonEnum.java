package javase.singleton;

/**
 * 枚举实现单例
 * @author Realfighter
 *
 */
public enum SingletonEnum {
	INSTANCE;
}
