package javase.singleton;

/**
 * 类加载器机制解决双重检查加锁问题 
 * 内部类Instance只被装载一次，
 * 保证对象SingletonDoubleCheckedByClassLoader只有一个
 * 
 * @author Realfighter
 * 
 */
public class SingletonDoubleCheckedByClassLoader {

	private static class Instance {
		static final SingletonDoubleCheckedByClassLoader instance = new SingletonDoubleCheckedByClassLoader();
	}

	private SingletonDoubleCheckedByClassLoader() {
	}

	public static SingletonDoubleCheckedByClassLoader getInstance() {
		return Instance.instance;
	}
}
