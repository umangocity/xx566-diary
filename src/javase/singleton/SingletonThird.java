package javase.singleton;

public enum SingletonThird {
	
	INSTANCE;
	
	public void test() {
		System.out.println("SingletonThird.test()");
	}
	
}
