package javase.singleton;

/**
 * 懒汉式单例
 * 
 * @author Realfighter
 * 
 */
public class SingletonIdler {

	private static SingletonIdler INSTANCE = null;

	private SingletonIdler() {
	}

	public synchronized static SingletonIdler getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new SingletonIdler();
		}
		return INSTANCE;
	}

}
