package javase.singleton;

/**
 * 饿汉式单例
 * 
 * @author Realfighter
 * 
 */
public class SingletonStarving {

	private static final SingletonStarving INSTANCE = new SingletonStarving();

	private SingletonStarving() {
	}

	public static SingletonStarving getInstance() {
		return INSTANCE;
	}

}
