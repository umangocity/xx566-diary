package javase.singleton;

public class SingletonFirst {

	public static final SingletonFirst INSTANCE = new SingletonFirst();

	private SingletonFirst() {
	}

	public void test() {
		System.out.println("SingletonFirst.test()");
	}

}
