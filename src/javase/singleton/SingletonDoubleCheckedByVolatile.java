package javase.singleton;

/**
 * volatile关键字保证，
 * INSTANCE变量被初始化为Singleton实例时，
 * 多个线程能正确的处理INSTANCE变量
 * 
 * @author Realfighter
 * 
 */
public class SingletonDoubleCheckedByVolatile {

	private volatile static SingletonDoubleCheckedByVolatile INSTANCE = null;

	private SingletonDoubleCheckedByVolatile() {
	}

	public static SingletonDoubleCheckedByVolatile getInstance() {
		if (INSTANCE == null) {
			synchronized (SingletonDoubleCheckedByVolatile.class) {
				if (INSTANCE == null) {
					INSTANCE = new SingletonDoubleCheckedByVolatile();
				}
			}
		}
		return INSTANCE;
	}
}
