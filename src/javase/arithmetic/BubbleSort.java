package javase.arithmetic;

import org.junit.Test;

import java.util.Arrays;

/**
 * 冒泡排序
 * User: Realfighter
 * Date: 2015/6/16
 * Time: 10:20
 */
public class BubbleSort {

    /**
     * 倒序
     */
    @Test
    public void testBubbleSort() {
        int[] array = {3, 8, 7, 2, 4, 8, 6};
        int temp;
        for (int i = 1, length = array.length; i < length; i++) {
            for (int j = 0; j < length - i; j++) {
                if (array[j] < array[j + 1]) {
                    temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }

    /**
     * 正序
     */
    @Test
    public void testBubbleSort2() {
        int[] array = {3, 8, 7, 2, 4, 8, 6};
        int temp;
        for (int i = 1, length = array.length; i < length; i++) {
            for (int j = 0; j < length - i; j++) {
                if (array[j] > array[j + 1]) {
                    temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }

    @Test
    public void testBubbleSort3() {
        String[] names = {"zhangsan", "lisi", "wangwu", "zhaoliu"};
        double[] scores = {33.4, 96.7, 88.5, 100};
        Student[] students = new Student[names.length];
        for (int i = 0; i < names.length; i++) {
            Student student = new Student();
            student.setName(names[i]);
            student.setScore(scores[i]);
            students[i] = student;
        }
        Student temp;
        for (int i = 1, length = students.length; i < length; i++) {
            for (int j = 0; j < length - i; j++) {
                if (students[j].getScore() < students[j + 1].getScore()) {
                    temp = students[j];
                    students[j] = students[j + 1];
                    students[j + 1] = temp;
                }
            }
        }
        for (Student student : students) {
            System.out.println(student.toString());
        }
    }


    class Student {
        String name;
        double score;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double getScore() {
            return score;
        }

        public void setScore(double score) {
            this.score = score;
        }

        @Override
        public String toString() {
            return "Student{" +
                    "name='" + name + '\'' +
                    ", score=" + score +
                    '}';
        }
    }

    /**
     * 正序去重
     */
    @Test
    public void testBubbleSort4() {
        int[] array = {20, 40, 32, 67, 40, 20, 89, 300, 400, 15};
        int temp;
        for (int i = 1, length = array.length; i < length; i++) {
            for (int j = 0; j < length - i; j++) {
                if (array[j] > array[j + 1]) {
                    temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        System.out.print(array[0] + " ");
        for (int i = 1; i < array.length; i++) {
            if (array[i] != array[i - 1]) {
                System.out.print(array[i] + " ");
            }
        }
    }

}
