package javase.arithmetic;

import org.junit.Test;

/**
 * 最快最简单的排序--桶排序(简化版)
 * User: Realfighter
 * Date: 2015/6/16
 * Time: 10:20
 */
public class BucketSort {

    @Test
    public void testBucketSort() {
        int[] array = {3, 8, 7, 2, 4, 8, 6};
        int[] original = new int[10];
        for (int i = 0, length = array.length; i < length; i++) {
            original[array[i]]++;
        }
        for (int i = 0, length = original.length; i < length; i++) {
            for (int j = 1; j <= original[i]; j++) {
                System.out.print(i + " ");
            }
        }
    }

    /**
     * 去重排序
     */
    @Test
    public void testBucketSort2() {
        int[] array = {20, 40, 32, 67, 40, 20, 89, 300, 400, 15};
        int[] original = new int[1000];
        for (int i = 0, length = array.length; i < length; i++) {
            if (original[array[i]] == 0) {
                original[array[i]]++;
            }
        }
        for (int i = 0, length = original.length; i < length; i++) {
            for (int j = 1; j <= original[i]; j++) {
                System.out.print(i + " ");
            }
        }
    }

    /**
     * 去重排序2
     */
    @Test
    public void testBucketSort3() {
        int[] array = {20, 40, 32, 67, 40, 20, 89, 300, 400, 15};
        int[] original = new int[1000];
        for (int i = 0, length = array.length; i < length; i++) {
            original[array[i]] = 1;
        }
        for (int i = 0, length = original.length; i < length; i++) {
            if (original[i] == 1) {
                System.out.print(i + " ");
            }
        }
    }

}
