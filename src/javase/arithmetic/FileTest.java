package javase.arithmetic;

import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.io.Files;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * User: Realfighter
 * Date: 2015/3/10
 * Time: 18:06
 */
public class FileTest {

    /**
     * 编写一个程序 将a.txt文件中的单词与b.txt文件中的单词交替合并到c.txt文件中 a.txt文件中的单词用回车符分隔，
     * b.txt文件中用回车或空格进行分隔。
     */
    //a.txt 									//b.txt
    /**
     i											this is a java program
     love										my name is Realfighter
     u
     baby
     */
    public static void main(String[] args) throws IOException {
        //读取a.txt b.txt里的内容 转为List<String>
        String apath = FileTest.class.getClassLoader().getResource("a.txt").getPath();
        List<String> aList = Files.readLines(new File(apath), Charsets.UTF_8);
        String bpath = FileTest.class.getClassLoader().getResource("b.txt").getPath();
        List<String> bList = Files.readLines(new File(bpath), Charsets.UTF_8);

        List<String> aWords = aList;// a.txt里面所有的单词
        List<String> bWords = Lists.newArrayList(Splitter.on(" ").split(Joiner.on(" ").join(bList)));// b.txt里面所有的单词
        List<String> bigOne = aWords.size() >= bWords.size() ? aWords : bWords;
        List<String> smallOne = aWords.size() >= bWords.size() ? bWords : aWords;

        StringBuffer from = new StringBuffer();
        for (int i = 0; i < smallOne.size(); i++) {
            from.append(bigOne.get(i)).append(" ").append(smallOne.get(i)).append(" ");
        }
        for (int j = smallOne.size(); j < bigOne.size(); j++) {
            from.append(bigOne.get(j)).append(" ");
        }
        // 写入文件
        String cpath = FileTest.class.getClassLoader().getResource("c.txt").getPath();
        File file = new File(cpath);
        Files.write(from, file, Charsets.UTF_8);
    }

}
