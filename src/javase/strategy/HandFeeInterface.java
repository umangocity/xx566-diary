package javase.strategy;

/**
 * 手续费接口
 * 
 * @Package javase.strategy
 * @ClassName: HandFeeInterface
 * @author Realfighter
 * @date 2014-8-8 下午02:31:09
 */
public interface HandFeeInterface {

	/**
	 * 手续费计算方法
	 * 
	 * @Title: countHandFee
	 * @author Realfighter
	 * @date 2014-8-8 下午02:32:03
	 * @param amount
	 *            交易金额
	 * @return double 应收手续费
	 */
	public double countHandFee(double amount);

}
