package javase.strategy;

/**
 * 手续费类
 * 
 * @Package javase.strategy
 * @ClassName: HandFee
 * @author Realfighter
 * @date 2014-8-8 下午03:11:12
 */
public class HandFee {

	// 手续费规则策略对象
	private HandFeeInterface handFeeInterface;

	/**
	 * <p>
	 * Description: 构造具体的手续费规则对象
	 * </p>
	 * 
	 * @param handFeeInterface
	 */
	public HandFee(HandFeeInterface handFeeInterface) {
		this.handFeeInterface = handFeeInterface;
	}

	public double countHandFee(double amount) {
		return handFeeInterface.countHandFee(amount);
	}

}
