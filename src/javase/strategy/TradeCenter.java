package javase.strategy;

/**
 * 交易中心
 * 
 * @Package javase.strategy
 * @ClassName: TradeCenter
 * @author Realfighter
 * @date 2014-8-8 下午03:07:54
 */
public class TradeCenter {

	public static void main(String[] args) {
		// 假设交易金额500元
		double amount = 500.0;
		
		// 选择合适的手续费规则策略
//		HandFeeInterface handFeeInterface = new HandFeeOfPC();//端游
//		HandFeeInterface handFeeInterface = new HandFeeOfPage();//页游
		HandFeeInterface handFeeInterface = new HandFeeOfMobile();//手游
		
		// 构造相应的手续费规则对象
		HandFee handFee = new HandFee(handFeeInterface);
		
		// 计算相应的手续费规则
		System.out.println(handFee.countHandFee(amount));
		
	}

}

/**
 * 端游手续费规则5%
 * 
 * @Package javase.strategy
 * @ClassName: HandFeeOfPC
 * @author Realfighter
 * @date 2014-8-8 下午02:34:18
 */
class HandFeeOfPC implements HandFeeInterface {

	/**
	 * 计算手续费
	 * 
	 * @Title: countHandFee
	 * @author Realfighter
	 * @date 2014-8-8 下午02:35:05
	 * @param amount
	 * @return
	 */
	public double countHandFee(double amount) {
		return amount * 5 / 100;
	}

}

/**
 * 页游手续费规则2%
 * 
 * @Package javase.strategy
 * @ClassName: HandFeeOfPage
 * @author Realfighter
 * @date 2014-8-8 下午03:05:05
 */
class HandFeeOfPage implements HandFeeInterface {

	/**
	 * 计算手续费
	 * 
	 * @Title: countHandFee
	 * @author Realfighter
	 * @date 2014-8-8 下午02:35:05
	 * @param amount
	 * @return
	 */
	public double countHandFee(double amount) {
		return amount * 2 / 100;
	}

}

/**
 * 手游手续费规则0
 * 
 * @Package javase.strategy
 * @ClassName: HandFeeOfMobile
 * @author Realfighter
 * @date 2014-8-8 下午03:05:50
 */
class HandFeeOfMobile implements HandFeeInterface {

	/**
	 * 计算手续费
	 * 
	 * @Title: countHandFee
	 * @author Realfighter
	 * @date 2014-8-8 下午02:35:05
	 * @param amount
	 * @return
	 */
	public double countHandFee(double amount) {
		return 0;
	}

}
