package javase.strategy;

/**
 * 手续费计算
 * 
 * @Package javase.strategy
 * @ClassName: HandFeeCaculator
 * @author Realfighter
 * @date 2014-8-8 下午04:08:52
 */
public enum HandFeeCaculator {

	// 端游
	PC {
		public double count(double amount) {
			return amount * 5 / 100;
		}
	},
	// 页游
	PAGE {
		public double count(double amount) {
			return amount * 2 / 100;
		}
	},
	// 手游
	MOBILE {
		public double count(double amount) {
			return 0.0;
		}
	};

	/**
	 * 手续费计算的抽象方法
	 * 
	 * @Title: count
	 * @author Realfighter
	 * @date 2014-8-8 下午04:22:53
	 * @param amount
	 *            交易金额
	 * @return double
	 */
	public abstract double count(double amount);

	//简单测试
	public static void main(String[] args) {
		// 交易金额
		double amount = 500.0;
		// 计算不同游戏类型的手续费
		System.out.println(HandFeeCaculator.PC.count(amount));

		System.out.println(HandFeeCaculator.PAGE.count(amount));

		System.out.println(HandFeeCaculator.MOBILE.count(amount));
	}

}
