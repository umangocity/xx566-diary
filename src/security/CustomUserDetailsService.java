package security;

import org.springframework.security.core.userdetails.UserDetailsService;

public abstract interface CustomUserDetailsService extends UserDetailsService {
}
