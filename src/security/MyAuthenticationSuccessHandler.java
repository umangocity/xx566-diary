package security;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;

@SuppressWarnings("rawtypes")
public class MyAuthenticationSuccessHandler implements ApplicationListener {


    public void onApplicationEvent(ApplicationEvent event) {
        // 登陆密码输入正确
        if ((event instanceof AuthenticationSuccessEvent)) {
            AuthenticationSuccessEvent authEvent = (AuthenticationSuccessEvent) event;
            // 登录的账户信息
            SecurityUser user = (SecurityUser) authEvent.getAuthentication().getPrincipal();
            // 判断用户状态
            // 更新用户登录信息
        }

        // 用户名密码错误，要进行用户登录限制
        if ((event instanceof AuthenticationFailureBadCredentialsEvent)) {
            // 登录的错误信息
            AuthenticationFailureBadCredentialsEvent authEvent = (AuthenticationFailureBadCredentialsEvent) event;
            // 用户的登录信息
            SecurityUser user = (SecurityUser) authEvent.getException().getExtraInformation();
            // 密码错误，比如需要锁定用户
            // 可以根据用户名查询数据库获取真实账户信息
        }
    }

}
