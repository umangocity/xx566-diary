package security.impl;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import security.CustomUserDetailsService;
import security.SecurityUser;


@Service("customUserDetailsService")
public class CustomUserDetailsServiceImpl implements CustomUserDetailsService {

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        //这里重写了loadUserByUsername方法，这里可以构造用户的权限，操作数据库等
        return new SecurityUser();
    }
}
