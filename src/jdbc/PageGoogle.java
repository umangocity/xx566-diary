package jdbc;

import java.util.List;

@SuppressWarnings("all")
public class PageGoogle {
	private int rowCount;// 数据库一共有多少行
	private int navCount;// 计算出一共有多少页
	private int pageSize;// 每页多少条
	private int next;// 下一页
	private int prev;// 上一页
	private int begin;// 导航起始
	private int end;// 导航结束
	private int num;// 当前页
	private int first = 1;// 第一页
	private int last;// 最后一页
	private int navNum = 1;// 有多少个导航
	private List<?> pageData;// 用于存放分页之后的数据

	public PageGoogle() {
	};// 无参构造函数

	// 构造函数,分页中最关键的三个属性(当前页、总记录数、每页数)
	public PageGoogle(int num, int rowCount, int pageSize) {
		this.rowCount = rowCount;
		this.pageSize = pageSize;
		this.navCount = (int) Math.ceil(this.rowCount * 1.0 / pageSize);
		this.last = this.navCount;
		// 限制当前页范围
		this.num = Math.max(this.first, num);
		this.num = Math.min(this.last, this.num);
		this.next = Math.min(this.last, this.num + 1);// 计算下一页
		this.prev = Math.max(this.first, this.num - 1);// 计算上一页
		this.begin = Math.max(this.num - navNum / 2, this.first);
		this.end = Math.min(this.begin + this.navNum - 1, this.last);
		if (end - begin < 9) {
			begin = Math.max(this.first, this.end - this.navNum + 1);
		}
	}

	public int getRowCount() {
		return rowCount;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	public int getNavCount() {
		return navCount;
	}

	public void setNavCount(int navCount) {
		this.navCount = navCount;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getNext() {
		return next;
	}

	public void setNext(int next) {
		this.next = next;
	}

	public int getPrev() {
		return prev;
	}

	public void setPrev(int prev) {
		this.prev = prev;
	}

	public int getBegin() {
		return begin;
	}

	public void setBegin(int begin) {
		this.begin = begin;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public int getFirst() {
		return first;
	}

	public void setFirst(int first) {
		this.first = first;
	}

	public int getLast() {
		return last;
	}

	public void setLast(int last) {
		this.last = last;
	}

	public int getNavNum() {
		return navNum;
	}

	public void setNavNum(int navNum) {
		this.navNum = navNum;
	}

	public List<?> getPageData() {
		return pageData;
	}

	public void setPageData(List<?> pageData) {
		this.pageData = pageData;
	}
	
}
