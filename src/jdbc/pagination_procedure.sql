CREATE OR REPLACE PROCEDURE P_Pagination_Util(--存储过程分页工具
	sqlString VARCHAR2,--查询语句
	page  int,--第几页
	perPageCount int,--每页几条记录
	totalPage out SYS_REFCURSOR,--查询出的总页数
	pageResultSet out SYS_REFCURSOR--当前页查询出来的结果集
)
AS
	pageSql varchar(2000);--查询某页结果的SQL语句
BEGIN
	open totalPage for 'select count(1) total from ('||sqlString||')';--查询出总数
	pageSql := 'select * from (select rownum rn,t.* from ('||sqlString||') t where rownum<='||(page*perPageCount)||')  where rn > '||(page-1)||'*'||perPageCount;
	open pageResultSet for pageSql;
END P_Pagination_Util; 
