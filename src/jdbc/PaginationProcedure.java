package jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PaginationProcedure {

	/**
	 * 分页查询数据
	 * @param sql 
	 * @param currentPage 当前页
	 * @param pageSize 每页条数
	 * @return
	 */
	public PageGoogle findList4Page(String sql, int currentPage, int pageSize) {
		String url = "jdbc:oracle:thin:@localhost:1521:orcl";
		String user = "test";
		String password = "test";
		Connection conn = null;// 数据库连接对象
		CallableStatement cs = null;// 存储过程对象
		ResultSet rs = null;// 结果集，返回分页后的数据
		ResultSet rs2 = null;// 结果集，返回查询的总行数
		Object object = null;// 需要放入list的对象
		PageGoogle page = null;// 分页对象
		int total = 0;// 记录总数
		int pagenum = 0;// 总页数
		List<Object> list = new ArrayList<Object>();
		try {
			conn = DriverManager.getConnection(url, user, password);
			;// 建立数据库连接
			/**
			 * 调用分页存储过程
			 */
			cs = conn.prepareCall("call P_Pagination_Util(?,?,?,?,?)");
			cs.setString(1, sql);// 传入查询语句
			cs.setInt(2, currentPage);// 当前第几页
			cs.setInt(3, pageSize);// 当前每页多少条
			cs.registerOutParameter(4, oracle.jdbc.OracleTypes.CURSOR);// 查询出来的总数
			cs.registerOutParameter(5, oracle.jdbc.OracleTypes.CURSOR);// 当前页查询出来的结果集
			cs.execute();// 执行存储过程
			rs2 = (ResultSet) cs.getObject(4);// 查询出的总数
			while (rs2.next()) {
				total = Integer.parseInt(rs2.getString("total"));
			}
			// 以当前页码,总数据量,每页显示多少条为参数构造分页对象
			page = new PageGoogle(currentPage, total, pageSize);
			System.out.println("记录总数--->" + total);
			pagenum = (int) Math.ceil(total / pageSize) + 1;
			System.out.println("总页数--->" + pagenum);
			rs = (ResultSet) cs.getObject(5);// 获取当前页的结果集
			while (rs.next()) {
				object = new Object();
				// 读取结果集封装对象
				// ...
				list.add(object);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (cs != null) {
					cs.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		page.setPageData(list);// 放入分页后的数据
		return page;
	}
}
