package guava;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Range;

/**
 * Range：为一个可比较类型连续的值定义连续边界
 * User: Realfighter
 * Date: 2014/10/16
 * Time: 22:18
 */
public class RangeTest {


    public static void main(String[] args) {
        //定义一个返回Person年龄的函数
        Function<Person, Integer> ageFunction = new Function<Person, Integer>() {
            @Override
            public Integer apply(Person input) {
                return input.getAge();
            }
        };
        //定义一个年龄大于等于25小于30岁的区间
        Range<Integer> ageRange = Range.closedOpen(25, 30);
        //定义一个对于指定区间年龄Person的过滤
        // 注：Range实现了Predicate接口，所以compose方法可以接口ageRange
        Predicate<Person> personPredicate = Predicates.compose(ageRange, ageFunction);
        Person person = new Person("张三", 30);
        System.out.println(personPredicate.apply(person));//false，不在区间内
    }

}

//Person实现了Comparable接口，当我们需要选择一定年龄区间的Person时
// 通过Comparable比较显然麻烦许多，这时候可以使用Range
class Person implements Comparable<Person> {
    private Integer age;//年龄
    private String name;//名称

    @Override
    public int compareTo(Person o) {
        //使用Guava提供的ComparisonChain进行对象的链式比较
        return ComparisonChain.start()
                .compare(this.name, o.getName())
                .compare(this.age, o.getAge()).result();
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    Person(String name, Integer age) {
        this.name = name;
        this.age = age;
    }
}
