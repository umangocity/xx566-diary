package guava;

import com.google.common.base.Ticker;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

/**
 * CacheBuilder：构造Cache和LoadingCache实例
 * User: Realfighter
 * Date: 2014/12/3
 * Time: 14:04
 */
public class CacheBuilderTest {

    @Test
    public void testFirst() {
        LoadingCache<String, TradeAccount> tradeAccountCache =
                CacheBuilder.newBuilder()
                        .expireAfterWrite(5L, TimeUnit.MINUTES)
                        .maximumSize(5000L)
                        //to fix the compile bug for testing
//                        .removalListener(new TradeAccountRemovalListener())
                        .ticker(Ticker.systemTicker())
                        .build(new CacheLoader<String, TradeAccount>() {
                            @Override
                            public TradeAccount load(String key) throws
                                    Exception {
                                return null;//to fix the compile bug for testing
//                                return
//                                        tradeAccountService.getTradeAccountById(key);
                            }
                        });
    }


    @Test
    public void testSecond() {
        LoadingCache<String, Book> bookCache = CacheBuilder.newBuilder()
                .expireAfterAccess(20L, TimeUnit.MINUTES)
                .softValues()
                //to fix the compile bug for testing
//                .removalListener(new BookRemovalListener())
                .build(new CacheLoader<String, Book>() {
                    @Override
                    public Book load(String key) throws Exception {
                        return null;//to fix the compile bug for testing
//                        return bookService.getBookByIsbn(key);
                    }
                });
    }

    @Test
    public void testFinal() {
        LoadingCache<String, TradeAccount> tradeAccountCache =
                CacheBuilder.newBuilder()
                        .concurrencyLevel(10)
                        .refreshAfterWrite(5L, TimeUnit.SECONDS)
                        .ticker(Ticker.systemTicker())
                        .build(new CacheLoader<String,
                                TradeAccount>() {
                            @Override
                            public TradeAccount load(String key)
                                    throws Exception {
                                return null;//to fix the compile bug for testing
//                                return
//                                        tradeAccountService.getTradeAccountById(key);
                            }
                        });
    }

    public class TradeAccount {
        private String id; //ID
        private String owner; //所有者
        private double balance; //余额
    }

    public class Book{
    }

}
