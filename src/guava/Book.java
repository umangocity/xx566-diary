package guava;

/**
 * User: Realfighter
 * Date: 2015/5/5
 * Time: 18:17
 */
public class Book {
    String isbn;
    double price;
    String title;
    String author;
    String publisher;

    public Book(Builder builder) {
        this.isbn = builder.isbn;
        this.price = builder.price;
        this.title = builder.title;
        this.author = builder.author;
        this.publisher = builder.publisher;
    }

    public static class Builder {
        String isbn = "yes";
        double price = 0.0;
        String title;
        String author;
        String publisher;

        public Builder title(String title) {
            this.title = title;
            return this;
        }


        public Builder author(String author) {
            this.author = author;
            return this;
        }

        public Builder publisher(String publisher) {
            this.publisher = publisher;
            return this;
        }

        public Builder isbn(String isbn) {
            this.isbn = isbn;
            return this;
        }

        public Builder price(double price) {
            this.price = price;
            return this;
        }

        public Book build() {
            return new Book(this);
        }
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
}
