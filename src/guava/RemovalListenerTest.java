package guava;

import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalListeners;
import com.google.common.cache.RemovalNotification;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * RemovalListener
 * User: Realfighter
 * Date: 2014/12/21
 * Time: 12:29
 */
public class RemovalListenerTest {

    private ExecutorService executor;
    private ListeningExecutorService executorService;

    @Before
    public void setUp() {
        executor = Executors.newCachedThreadPool();
        executorService = MoreExecutors.listeningDecorator(executor);
    }

    @Test
    public void testRemovalListener() {
        RemovalListener<String, TradeAccount> myRemovalListener = new
                RemovalListener<String, TradeAccount>() {
                    @Override
                    public void onRemoval(RemovalNotification<String,
                            TradeAccount> notification) {
                        //Do something here
                    }
                };
        RemovalListener<String, TradeAccount> removalListener =
                RemovalListeners.asynchronous(myRemovalListener, executorService);
    }

    class TradeAccount {
    }

}
