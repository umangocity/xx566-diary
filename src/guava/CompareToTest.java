package guava;

import com.google.common.collect.ComparisonChain;
import org.junit.Test;

/**
 * CompareTo：两个对象进行比较时，需要实现此接口
 * Guava中提供了ComparisonChain
 * User: Realfighter
 * Date: 2014/8/20
 * Time: 10:39
 */
public class CompareToTest {

    //现在媒婆1和媒婆2都给我介绍了个女生，居然都叫lisa，我需要比较下是不是同一个人
    @Test
    public void testCompareTo() {
        Girl g1 = new Girl("lisa", 175.00, "nice");
        Girl g2 = new Girl("lisa", 175.00, "beauty");
        //两个girl的face不相同
        System.out.println(g1.compareTo(g2) == 0);//false
    }

    class Girl implements Comparable<Girl> {

        private String name;//名称
        private double height;//身高
        private String face;//长相

        Girl(String name, double height, String face) {
            this.name = name;
            this.height = height;
            this.face = face;
        }

        //使用Guava提供的ComparisonChain我们这样比较
        @Override
        public int compareTo(Girl girl) {
            return ComparisonChain.start()
                    .compare(name, girl.name)
                    .compare(height, girl.height)
                    .compare(face, girl.face)
                    .result();
        }

        //传统方法我们这样比较
//        @Override
//        public int compareTo(Girl girl) {
//            int c1 = name.compareTo(girl.name);
//            if (c1 != 0){
//                System.out.println("两个girl的name不相同");
//                return c1;
//            }
//            int c2 = Double.compare(height, girl.height);
//            if (c2 != 0){
//                System.out.println("两个girl的height不相同");
//                return c2;
//            }
//            int c3 = face.compareTo(girl.face);
//            if(c3 !=0)
//                System.out.println("两个girl的face不相同");
//            return c3;
//        }
    }
}
